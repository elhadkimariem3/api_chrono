-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : lun. 07 mars 2022 à 16:43
-- Version du serveur : 10.4.22-MariaDB
-- Version de PHP : 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `chronomobile`
--

-- --------------------------------------------------------

--
-- Structure de la table `genration_telephonne`
--

CREATE TABLE `genration_telephonne` (
  `id` int(11) NOT NULL,
  `nom` varchar(250) NOT NULL,
  `id_type_telephonne` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `genration_telephonne`
--

INSERT INTO `genration_telephonne` (`id`, `nom`, `id_type_telephonne`) VALUES
(1, 'A52', 2);

-- --------------------------------------------------------

--
-- Structure de la table `infos_telephonne`
--

CREATE TABLE `infos_telephonne` (
  `id` int(11) NOT NULL,
  `prix_initial` float NOT NULL,
  `stockage` varchar(250) NOT NULL,
  `genration_telephonne` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `infos_telephonne`
--

INSERT INTO `infos_telephonne` (`id`, `prix_initial`, `stockage`, `genration_telephonne`) VALUES
(1, 300, '128', 1);

-- --------------------------------------------------------

--
-- Structure de la table `type_telephonne`
--

CREATE TABLE `type_telephonne` (
  `id` int(11) NOT NULL,
  `Type` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `type_telephonne`
--

INSERT INTO `type_telephonne` (`id`, `Type`) VALUES
(1, 'iphone '),
(2, 'samsung');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `genration_telephonne`
--
ALTER TABLE `genration_telephonne`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_type_telephonne` (`id_type_telephonne`);

--
-- Index pour la table `infos_telephonne`
--
ALTER TABLE `infos_telephonne`
  ADD PRIMARY KEY (`id`),
  ADD KEY `genration_telephonne` (`genration_telephonne`);

--
-- Index pour la table `type_telephonne`
--
ALTER TABLE `type_telephonne`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `genration_telephonne`
--
ALTER TABLE `genration_telephonne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `infos_telephonne`
--
ALTER TABLE `infos_telephonne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `type_telephonne`
--
ALTER TABLE `type_telephonne`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `genration_telephonne`
--
ALTER TABLE `genration_telephonne`
  ADD CONSTRAINT `genration_telephonne_ibfk_1` FOREIGN KEY (`id_type_telephonne`) REFERENCES `type_telephonne` (`id`);

--
-- Contraintes pour la table `infos_telephonne`
--
ALTER TABLE `infos_telephonne`
  ADD CONSTRAINT `infos_telephonne_ibfk_1` FOREIGN KEY (`genration_telephonne`) REFERENCES `genration_telephonne` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
